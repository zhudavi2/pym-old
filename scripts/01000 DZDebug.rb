# Debug namespace
# @author BlasterMillennia
module DZDebug
  module LogLevel
    ALWAYS  = 1 << 0
    ERROR   = 1 << 1
    WARNING = 1 << 2
    INFO1   = 1 << 3
    INFO2   = 1 << 4
    ENTRY   = 1 << 5
    EXIT    = 1 << 6
    TRACE   = ENTRY | EXIT
  end

  DEFAULT_LOG_LEVEL = LogLevel::ALWAYS | LogLevel::ERROR | LogLevel::WARNING

  LOG_LEVEL_NAMES = {
    LogLevel::ALWAYS  => "ALWAYS",
    LogLevel::ERROR   => "ERROR",
    LogLevel::WARNING => "WARNING",
    LogLevel::INFO1   => "INFO1",
    LogLevel::INFO2   => "INFO2",
    LogLevel::ENTRY   => "TRACE",
    LogLevel::EXIT    => "TRACE",
  }

  @log_levels_enabled = {
    "Battle::Logic::ExpHandler" => LogLevel::TRACE | LogLevel::INFO1,
    "GamePlay::Save"            => 0,
    "PFM::Pokemon"              => 0,
    "PFM::PokemonBattler"       => 0,
    "PFM::Wild_Battle"          => LogLevel::TRACE | LogLevel::INFO1,
  }

  # Logging
  def self.log(level, caller, message)
    class_name = @log_levels_enabled.include?(caller.to_s) ? caller.to_s : caller.class.to_s 
    loop do
      if !@log_levels_enabled.include?(class_name)
        puts "Unrecognized caller = #{caller}, caller.name = #{caller.name}, caller.class = #{caller.class}, caller.class.name = #{caller.class.name}"
        break
      end
      log_levels_enabled = DEFAULT_LOG_LEVEL | (@log_levels_enabled[class_name] != nil ? @log_levels_enabled[class_name] : 0)
      if (log_levels_enabled & level) != 0
        trace_string = ""
        case level
          when LogLevel::ENTRY
            trace_string = ">>> "
          when LogLevel::EXIT
            trace_string = "<<< "
        end
        caller_location = caller_locations(3,1)[0]
        absolute_path = caller_location.absolute_path
        absolute_path = absolute_path.split("/scripts/")[1]
        puts "[#{LOG_LEVEL_NAMES[level]}] #{absolute_path} - #{class_name} - #{caller_location.label} - #{caller_location.lineno}: #{trace_string}#{message}"
      end
      break
    end
  end
end
