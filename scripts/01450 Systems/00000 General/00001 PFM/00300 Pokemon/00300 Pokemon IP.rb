# @author BlasterMillennia
module PFM
  class Pokemon
    # Return the list of IP the Pokémon gives when beaten
    # @return [Array<Integer>] ip list: [hp, atk, dfe, spd, ats, dfs]
    def ip_battle_list
      DZDebug.log(DZDebug::LogLevel::ENTRY, self, "")
      data = get_data
      list = [data.base_hp, data.base_atk, data.base_dfe, data.base_spd, data.base_ats, data.base_dfs]
      DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning #{list}")
      return list
    end

    # Add IP, for a full set of stats, to a Pokémon
    # @param list [Array<Integer>] an IP list: [hp, atk, dfe, spd, ats, dfs]
    # @return [Boolean, nil] if the ip had totally been added or not (nil = couldn't be added at all)
    def add_ip(list)
      DZDebug.log(DZDebug::LogLevel::ENTRY, self, "list = #{list}")
      result = nil
      if !egg?
        result = true
        list.each_with_index do |ip, stat|
          result &= add_stat_ip(stat, ip)
        end
      end
      DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning #{result}")
      return result
    end

    # Safely add IP per stat
    # @param stat [GameData::EV] Stat of IP to add
    # @param n [Integer] amount of IP to add
    # @return [Boolean] if the ip has successfully been added
    def add_stat_ip(stat, n)
      DZDebug.log(DZDebug::LogLevel::ENTRY, self, "stat = #{stat}, n = #{n}")
      result = true
      loop do
        break if n == 0
        stats = Configs.stats
        if self.ip[stat] > stats.max_stat_ip - 1
          result = false
          break
        end
        self.ip[stat] += n
        self.ip[stat].clamp(0, stats.max_stat_ip)
        if stat == stats.hp_index
          @hp = (@hp_rate * max_hp).round
          @hp_rate = @hp.to_f / max_hp
        end
        break
      end
      DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning #{result}")
      return result
    end
  end
end
