module PFM
  # @author BlasterMillennia
  class Pokemon
    def self.calculate_iv_from_ip(ip)
      [Math.sqrt(ip / 32).floor - 1, 0].max
    end 

    def max_hp
      return 1 if db_symbol == :shedinja
      iv_hp = Pokemon.calculate_iv_from_ip(self.ip[Configs.stats.hp_index])
      return ((iv_hp + 2 * base_hp + @ev_hp / 4) * @level) / 100 + 10 + @level
    end

    def atk_basis
      iv_atk = Pokemon.calculate_iv_from_ip(self.ip[Configs.stats.atk_index])
      return calc_regular_stat(base_atk, iv_atk, @ev_atk, 1)
    end

    def dfe_basis
      iv_dfe = Pokemon.calculate_iv_from_ip(self.ip[Configs.stats.dfe_index])
      return calc_regular_stat(base_dfe, iv_dfe, @ev_dfe, 2)
    end

    def spd_basis
      iv_spd = Pokemon.calculate_iv_from_ip(self.ip[Configs.stats.spd_index])
      return calc_regular_stat(base_spd, iv_spd, @ev_spd, 3)
    end

    def ats_basis
      iv_ats = Pokemon.calculate_iv_from_ip(self.ip[Configs.stats.ats_index])
      return calc_regular_stat(base_ats, iv_ats, @ev_ats, 4)
    end

    def dfs_basis
      iv_dfs = Pokemon.calculate_iv_from_ip(self.ip[Configs.stats.dfs_index])
      return calc_regular_stat(base_dfs, iv_dfs, @ev_dfs, 5)
    end

    # Return the IP HP text
    # @return [String]
    def ip_hp_text
      stat_ip = self.ip[Configs.stats.hp_index]
      format(ip_text, stat_ip, Pokemon.calculate_iv_from_ip(stat_ip))
    end

    # Return the IP ATK text
    # @return [String]
    def ip_atk_text
      stat_ip = self.ip[Configs.stats.atk_index]
      format(ip_text, stat_ip, Pokemon.calculate_iv_from_ip(stat_ip))
    end

    # Return the IP DFS text
    # @return [String]
    def ip_dfe_text
      stat_ip = self.ip[Configs.stats.dfe_index]
      format(ip_text, stat_ip, Pokemon.calculate_iv_from_ip(stat_ip))
    end

    # Return the IP SPD text
    # @return [String]
    def ip_spd_text
      stat_ip = self.ip[Configs.stats.spd_index]
      format(ip_text, stat_ip, Pokemon.calculate_iv_from_ip(stat_ip))
    end

    # Return the IP ATS text
    # @return [String]
    def ip_ats_text
      stat_ip = self.ip[Configs.stats.ats_index]
      format(ip_text, stat_ip, Pokemon.calculate_iv_from_ip(stat_ip))
    end

    # Return the IP DFS text
    # @return [String]
    def ip_dfs_text
      stat_ip = self.ip[Configs.stats.dfs_index]
      format(ip_text, stat_ip, Pokemon.calculate_iv_from_ip(stat_ip))
    end

    # Return the text "IP: %d (%d)"
    # @return [String]
    def ip_text
      'IP: %d (%d)'
    end
  end
end
