module PFM
  class Wild_Battle
    def setup(battle_id = 1)
      # If it was a forced battle
      return configure_battle(@forced_wild_battle, battle_id) if @forced_wild_battle
      # Security for when a Repel is used at the same time an encounter is happening
      return nil if PFM.game_state.repel_count > 0
      return nil unless (group = current_selected_group)

      @next_creatures = {} if @next_creatures == nil
      @next_creatures[group.id] = [] if !@next_creatures.include?(group.id)

      creature_to_select = configure_creature(@next_creatures[group.id])
      while creature_to_select.count { |_, rate| rate > 0 } < (group.is_double_battle ? 2 : 1) do
        @next_creatures[group.id] += generate_next_creatures(group.encounters)
        creature_to_select = configure_creature(@next_creatures[group.id])
      end

      selected_creature = select_creature(group, creature_to_select)
      selected_creature.each { |creature| @next_creatures[group.id].delete(creature) }
      return configure_battle(selected_creature, battle_id)
    ensure
      @forced_wild_battle = false
      @fish_battle = nil
    end

    # @param encounters [Array<Studio::Group::Encounter>]
    def generate_next_creatures(encounters)
      DZDebug.log(DZDebug::LogLevel::ENTRY, self, "encounters = #{encounters}")
      maxed = MAX_POKEMON_LEVEL_ABILITY.include?(creature_ability) && rand(100) < 50
      next_creatures = []
      # Rare
      next_creatures << encounters[0].to_creature(maxed ? encounters[0].level_setup.range.end : nil) if rand(39) == 0
      # Uncommon
      [*0...26].sample(2).each do |index|
        next_creatures << encounters[index].to_creature(maxed ? encounters[index].level_setup.range.end : nil) if (1..2).include?(index)
      end
      # Common
      [*0...27].sample(5).each do |index|
        next_creatures << encounters[index].to_creature(maxed ? encounters[index].level_setup.range.end : nil) if (3..7).include?(index)
      end
      # Reverse holo
      rev_holo_index = rand(82) 
      next_creatures << encounters[rev_holo_index].to_creature(maxed ? encounters[rev_holo_index].level_setup.range.end : nil) if (0...8).include?(rev_holo_index)

      next_creatures.each do |creature|
        DZDebug.log(DZDebug::LogLevel::INFO1, self, "Next creature #{creature.name} level #{creature.level}")
      end
      DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning #{next_creatures}")
      return next_creatures
    end
  end
end
