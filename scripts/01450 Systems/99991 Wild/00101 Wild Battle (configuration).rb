module PFM
  class Wild_Battle
    # @param creature_to_select [Array<Array(PFM::Pokemon, Float)>] One randomly-generated Pokémon per possible group encounter, random allowed level, with rate calculated based on items, abilities, etc. and not considering rate within group; rate might be 0, for example if Repel is active and randomly-generated level is too low
    def select_creature(group, creature_to_select)
      DZDebug.log(DZDebug::LogLevel::EXIT, self, "group = #{group}, creature_to_select = #{creature_to_select}")
      # @type [Array<Float>]
      reduced_rareness = creature_to_select.reduce([]) { |acc, curr| acc << (curr.last + (acc.last || 0)) }
      max_rand = reduced_rareness.last
      # This reducer prevents to select the exact same Creature twice
      creatures = (group.is_double_battle ? 2 : 1).times.reduce([]) do |acc, _|
        nb = Random::WILD_BATTLE.rand(max_rand.to_i)
        index = reduced_rareness.find_index { |i| i > nb } || creature_to_select.size - 1
        creature = creature_to_select[index].first
        redo if acc.include?(creature)
        acc << creature
      end
      DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning #{creatures}")
      return creatures
    end
  end
end
