# @author BlasterMillennia
module Configs
  class Stats
    # Maximum amount of IP on a stat
    # @return [Integer]
    attr_accessor :max_stat_ip

    alias old_initialize initialize
    def initialize
      old_initialize
      @max_total_ev = 508
      @max_stat_ip = 32768
    end
  end
end
