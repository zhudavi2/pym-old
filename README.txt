How to use:
1. Download Studio. https://pokemonworkshop.com/en/studio
2. Create a new Studio project.
3. Overwrite the project folder's contents (there should a file named "project.studio") with this repository's contents.
4. Set up Ruby, Solargraph, and the scripts/psdk_scripts symbolic link as in https://www.youtube.com/watch?v=E9zYkR_3rbM
5. In the project folder, create a pokemonsdk symbolic link, pointing to the PSDK scripts that came with Studio. As administrator:
>mklink /d pokemonsdk %temp%\..\Programs\pokemon-studio\psdk-binaries\pokemonsdk

Credits:
- Studio credits: https://pokemonworkshop.com/en/help/pokemon-studio/who-is-developing-pokemon-studio
- PSDK Credits: https://pokemonworkshop.com/en/sdk/credits
